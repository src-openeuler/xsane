%global gimpplugindir %(gimptool --gimpplugindir 2>/dev/null || echo INVALID)/plug-ins
%global iconsdir %{_datadir}/icons/hicolor
%global _configure ../configure

%bcond_with gimp

Name: xsane
Version: 0.999
Release: 32
Summary: X Window System front-end for the SANE scanner interface
License: GPLv2+
URL: http://www.xsane.org/
Source0: https://fossies.org/linux/misc/xsane-0.999.tar.gz
Source1: xsane-256x256.png
Patch0000: xsane-0.995-xdg-open.patch
Patch0001: xsane-0.995-close-fds.patch
Patch0002: xsane-0.996-no-eula.patch
Patch0003: xsane-0.997-off-root-build.patch
Patch0004: xsane-0.999-no-file-selected.patch
Patch0005: xsane-0.997-ipv6.patch
Patch0006: xsane-0.998-preview-selection.patch
Patch0007: xsane-0.998-libpng.patch
Patch0008: xsane-0.998-wmclass.patch
Patch0009: xsane-0.998-desktop-file.patch
Patch0010: xsane-0.999-man-page.patch
Patch0011: xsane-0.999-pdf-no-high-bpp.patch
Patch0012: xsane-0.999-lcms2.patch
Patch0013: xsane-0.999-coverity.patch
Patch0014: xsane-0.999-snprintf-update.patch
Patch0015: xsane-0.999-signal-handling.patch
Patch0016: xsane-0.999-7-autoconf.patch
BuildRequires: gcc gtk2-devel lcms2-devel libjpeg-devel sane-backends-devel >= 1.0.19-15
BuildRequires: libtiff-devel gettext-devel desktop-file-utils >= 0.2.92
Requires: xsane-common hicolor-icon-theme hicolor-icon-theme

%description
XSane is an X based interface for the SANE (Scanner Access Now Easy)
library, which provides access to scanners, digital cameras, and other
capture devices. XSane is written in GTK+ and provides control for
performing the scan and then manipulating the captured image.

%if %{with gimp}
%package gimp
Summary: GIMP plug-in providing the SANE scanner interface
BuildRequires: gimp-devel
Requires: gimp >= 2:2.2.12-4
Requires: xsane-common

%description gimp
This package provides the regular XSane frontend for the SANE scanner
interface, but it works as a GIMP plug-in. You must have GIMP
installed to use this package.
%endif

%package common
Summary: Common files for xsane packages

%description common
This package contains common files needed by other xsane packages.

%package help
Summary: Documents for %{name}
Requires: man info

%description help
Man pages and other related documents for %{name}

%prep
for doc in xsane.{CHANGES,PROBLEMS,INSTALL}; do
    iconv -f ISO-8859-1 -t utf8 "$doc" -o "$doc.new" && \
    touch -r "$doc" "$doc.new" && \
    mv "$doc.new" "$doc"
done
%autosetup -p1
rm include/config.h
mkdir build-with-gimp
mkdir build-without-gimp

%build
CFLAGS='%optflags -fno-strict-aliasing -DXSANE_BUGTRACKER_URL=\"https://gitee.com/organizations/src-openeuler/issues\"'
export CFLAGS
%if %{with gimp}
pushd build-with-gimp
%configure --enable-gimp
make %{?_smp_mflags}
popd
%endif
pushd build-without-gimp
%configure --disable-gimp
make
popd
cp %{SOURCE1} src/

%install
pushd build-without-gimp
make DESTDIR=%{buildroot} install
popd
%if %{with gimp}
install -m 0755 -d %{buildroot}%{gimpplugindir}
install -m 0755 build-with-gimp/src/xsane %{buildroot}%{gimpplugindir}
%endif
rm %{buildroot}%{_datadir}/applications/xsane.desktop
desktop-file-install \
    --dir %{buildroot}%{_datadir}/applications \
    src/xsane.desktop

for res in 16 32 48 256; do
    tdir="%{buildroot}%{iconsdir}/${res}x${res}/apps"
    install -m 0755 -d "$tdir"
    install -m 0644 src/xsane-${res}x${res}.png "${tdir}/xsane.png"
done

mkdir -p $RPM_BUILD_ROOT%{_datadir}/appdata
cat > $RPM_BUILD_ROOT%{_datadir}/appdata/%{name}.appdata.xml <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<!-- Copyright 2014 Ryan Lerch <rlerch@redhat.com> -->
<!--
EmailAddress: Oliver.Rauch@xsane.org
SentUpstream: 2014-09-17
-->
<application>
  <id type="desktop">xsane.desktop</id>
  <metadata_license>CC0-1.0</metadata_license>
  <summary>Scan images with a scanner</summary>
  <description>
    <p>
      XSane is an application to scan images using a hardware scanner attached
      to your computer.
      It is able to save in a variety of image formats, including TIFF and JPEG
      and can even save your scan as a PDF.
      XSane also has support for scanning multiple pages and merging them into
      a single document.
    </p>
  </description>
  <url type="homepage">http://www.xsane.org/</url>
  <screenshots>
    <screenshot type="default">http://www.xsane.org/doc/xsane-save.jpg</screenshot>
  </screenshots>
</application>
EOF

%find_lang %{name} XSANE.lang

%if %{with gimp}
%pre gimp
if [ -L "%{gimpplugindir}/xsane" ]; then
    rm -f "%{gimpplugindir}/xsane"
fi
%endif

%files -f XSANE.lang
%doc xsane.ACCELKEYS xsane.AUTHOR xsane.BEGINNERS-INFO xsane.BUGS xsane.CHANGES xsane.FAQ xsane.LANGUAGES xsane.LOGO xsane.NEWS xsane.ONLINEHELP xsane.PROBLEMS xsane.ROOT xsane.TODO
%license xsane.COPYING
%{_bindir}/xsane
%{_datadir}/appdata/%{name}.appdata.xml
%{_datadir}/applications/xsane.desktop
%{_datadir}/pixmaps/xsane.xpm
%{iconsdir}/*/apps/%{name}.png

%if %{with gimp}
%files gimp
%{gimpplugindir}/xsane
%endif

%files common
%doc xsane.AUTHOR
%license xsane.COPYING
%dir %{_datadir}/sane
%{_datadir}/sane/xsane

%files help
%{_mandir}/man1/*

%changelog
* Tue Mar  2 2021 lingsheng <lingsheng@huawei.com> - 0.999-32
- Disable gimp plugin build

* Fri Sep  11 2020 baizhonggui <baizhonggui@huawei.com> - 0.999-31
- Modify source0

* Fri Apr  3 2020 songshuaishuai <songshuaishuai2@huawei.com> - 0.999-30
- fix XSANE_BUGTRACKER_URL

* Tue Nov 22 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.999-29 
- Package init

